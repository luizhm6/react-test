import { NextResponse } from "next/server";

export async function GET(req: any) {
    return NextResponse.json({time: new Date().toLocaleString()})
}
