import { NextResponse } from "next/server";
import dbConnect from "@/app/utils/db-connection";
import User from "@/app/models/user";
import bcrypt from "bcrypt";

export async function POST(req: any) {
    await dbConnect();
    let body = await req.json();
    let {name, email, password} = body;
    try {
        const user = await new User({
            name,
            email,
            password: await bcrypt.hash(password, 10)
        }).save()
        return NextResponse.json(user)        
    } catch (error: any) {
        console.log(error);
        return NextResponse.json({error: error.message}, {status: 500});
    }
}
