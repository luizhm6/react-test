import NextAuth from "next-auth/next";
import { authOptions } from "@/app/utils/authOptions";

const handler = NextAuth(authOptions);

export async function GET(req, res) {
  return NextAuth(req, res, authOptions);
}

export async function POST(req, res) {
  return NextAuth(req, res, authOptions);
}
