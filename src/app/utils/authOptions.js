import CredentialsProvider from "next-auth/providers/credentials";
import User from "../models/user";
import bcrypt from "bcrypt";
import dbConnect from "./db-connection";

export const authOptions = {
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialsProvider({
      async authorize(credentials, req) {
        console.log("connections");
        dbConnect();
        const { email, password } = credentials;
        const user = await User.findOne({ email });
        console.log(user, "user credentials");
        if (!user) {
          throw new Error("Email ou senha invalidos");
        }

        const isPasswordMached = await bcrypt.compare(password, user.password);
        if (!isPasswordMached) {
          throw new Error("Email ou senha invalidos");
        }

        return user;
      },
    }),
  ],
  secret: process.env.NEXTAUTH_SECRET,
  pages: {
    signIn: "/login",
  },
};
