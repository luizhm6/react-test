import mongoose from "mongoose";
import { env } from "process";

const dbConnect = async () => {
    console.log('conectando')
    if(mongoose.connection.readyState >= 1){
        console.log('ja estava conectado')
        return;
    }
    mongoose.connect(process.env.DB_URI || '')
    console.log('conectado agora')
};

export default dbConnect;