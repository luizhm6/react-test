import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Nome é obrigatório"],
        trim: true,
        minLength: 3,
        maxLength: 50 
    },
    email: {
        type: String,
        required: [true, "Email é obrigatório"],
        index: true,
        lowercase: true,
        unique: true,
        trim: true,
        minLength: 3
    },
    password: String,
    role: {
        type: String,
        default: 'user',
    },
    image: String,
    resetCode: {
        data: String,

    }
}, {timestamps: true})

userSchema.plugin(uniqueValidator);

export default mongoose.models.User || mongoose.model("User", userSchema);
